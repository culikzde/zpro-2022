#include <iostream>
using namespace std;

int fact1 (int m)
{
    int product = 1;
    for (int k = 1; k <= m; k++)
    {
        product = product * k;
    }
    return product;
}

int fact2(int m)
{
    int product = 1;
    while (m >= 1)
    {
        product = product * m;
        m--;
    }
    return product;
}

int g = 1;

int fact(int n)
{
    if (n > 1)
        return n * fact(n - 1);
    else
        return 1;
}

int fib0 (int n)
{
    if (n == 0)
        return 0;
    else if (n == 1)
        return 1;
    else
        return fib0 (n-1) + fib0 (n-2);
}

const int MAX = 10;
const int NEZNAM = -1;
int pam [MAX];

int fib (int n)
{
    if (n == 0)
        return 0;
    else if (n == 1)
        return 1;
    else
    {
        if (n >= 0 && n < MAX)
            if (pam[n] != NEZNAM)
                return pam[n];

        int v = fib0(n - 1) + fib0(n - 2);
        if (n >= 0 && n < MAX)
            pam[n] = v;
        return v;

    }
}

int main()
{
    for (int k = 0; k < MAX; k++)
    {
        pam[k] = NEZNAM;
    }

    for (int n = 0; n <= 12; n++)
    {
        cout << "Fib " << n << " = " << fib (n) << endl;
    }

    /*
    double product = 1;
    for (int k = 1 ; k <= n ; k++)
    {
        product = product * k;
        // product *= k;
        cout << "faktorial " << k << " = " << product << endl;
    }

    cout << endl;
    cout << "Faktorial " << n << " = " << product << endl;
    */
}

