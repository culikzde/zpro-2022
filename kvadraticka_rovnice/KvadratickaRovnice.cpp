#include <iostream>
#include <complex>
using namespace std;

int main()
{
    double a, b, c;

    if (false)
    {
        cout << "Vlozte cislo a: ";
        cin >> a;

        cout << "Vlozte cislo b: ";
        cin >> b;

        cout << "Vlozte cislo c: ";
        cin >> c;
    }
    else
    {
        a = 1;
        b = 0;
        c = 4;
    }

    double D = b * b - 4 * a * c;

    if (D >= 0)
    {
        double x1 = (-b + sqrt(D)) / (2 * a);
        double x2 = (-b - sqrt(D)) / (2 * a);

        cout << "x1 = " << x1 << endl;
        cout << "x2 = " << x2 << endl;
    }
    else
    {
        double re = -b / (2 * a);
        double im = sqrt(-D) / (2 * a);

        if (im >= 0)
        {
            cout << "x1 = " << re << " + " << im << " i " << endl;
            cout << "x2 = " << re << " - " << im << " i " << endl;
        }
        else
        {
            cout << "x1 = " << re << " - " << -im << " i " << endl;
            cout << "x2 = " << re << " + " << -im << " i " << endl;

        }
    }

    cout << "O.K." << endl;
}


