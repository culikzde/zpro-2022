#include <iostream>
#include <complex>
using namespace std;

int main()
{
    double a, b, c;

    if (false)
    {
        cout << "Vlozte cislo a: ";
        cin >> a;

        cout << "Vlozte cislo b: ";
        cin >> b;

        cout << "Vlozte cislo c: ";
        cin >> c;
    }
    else
    {
        a = 1;
        b = 0;
        c = 4;
    }

    complex <double> D = b * b - 4 * a * c;

    complex <double> x1 = (-b + sqrt(D)) / (2 * a);
    complex <double> x2 = (-b - sqrt(D)) / (2 * a);

    cout << "x1 = " << x1 << endl;
    cout << "x2 = " << x2 << endl;
    cout << "O.K." << endl;
}


