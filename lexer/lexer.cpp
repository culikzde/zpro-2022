

#include "lexer.h"

#include <iostream>
using namespace std;

const char zero = 0;

Lexer::Lexer(string fileName)
{
    f.open(fileName);
    if (not f.good())
        error("Cannot open file: " + fileName);

    ch = zero;
    nextChar();
    nextToken();
}

Lexer::~Lexer()
{
    if (f.good())
        f.close();
}

void Lexer::error(string msg)
{
    cerr << "Error: " << msg << ", token=" << token << endl;
    exit(1);
}

void Lexer::nextChar()
{
    f >> ch;
    if (!f.good())
        ch = zero;
}

void Lexer::nextToken()
{
    token = "";
    while (ch != zero && ch <= ' ')
        nextChar();

    if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
    {
        kind = ident;
        while (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
        {
            token = token + ch;
            nextChar();
        }
    }
    else if (ch >= '0' && ch <= '9')
    {
        kind = number;
        while (ch >= '0' && ch <= '9')
        {
            token = token + ch;
            nextChar();
        }
    }
    else if (ch != zero)
    {
        kind = separator;
        token = string(1, ch);
        nextChar();
    }
    else
    {
        kind = eos;
    }
}

bool Lexer::isSeparator(char c)
{
    return kind == separator && token == string(1, c);
}

void Lexer::checkSeparator(char c)
{
    if (!isSeparator(c))
        error(string(1, c) + " expected");
    nextToken();
}

double Lexer::readNumber()
{
    if (kind != number)
        error("Number expected");

    double result = 0;

    /*
    for (int i = 0 ; i < token.length() ; i++)
    {
        char c = token [i];
        ...
    }
    */

    for (char c : token)
    {
        result = 10 * result + (c - '0');
    }

    nextToken();
    return result;
}

