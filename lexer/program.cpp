#include <iostream>
using namespace std;
#include "lexer.h"

const int R = 3;
const int S = 4;
typedef double Matice[R][S];

void readMat(Lexer& inp, Matice m)
{
    inp.checkSeparator('{');
    int i = 0;
    while (!inp.isSeparator('}'))
    {
        inp.checkSeparator('[');
        if (i >= R) inp.error("Too many lines");
        int k = 0;
        while (!inp.isSeparator(']'))
        {
            double num = inp.readNumber();
            if (k >= S) inp.error("Too many columns");
            m[i][k] = num;
            k++;

            if (!inp.isSeparator(']'))
                inp.checkSeparator(',');
        }
        inp.checkSeparator(']');
        if (k != S) inp.error("Missing column");

        i++;

        if (inp.isSeparator(','))
            inp.nextToken();
    }
    inp.checkSeparator('}');
    if (i != R) inp.error("Missing line");
}

void printMat(Matice m)
{
    for (int i = 0; i < R; i++)
    {
        for (int k = 0; k < S; k++)
        {
            cout << m[i][k];
            if (k < S - 1) cout << ", ";
        }
        cout << endl;
    }
    cout << endl;
}

int main()
{
    Matice a, b;

    Lexer lex("abc.txt");
    readMat(lex, a);

    char op = ' ';
    if (lex.isSeparator('+'))
    {
        op = lex.token[0];
        lex.nextToken();
    }
    else
    {
        lex.error("Unknown op");
    }

    readMat(lex, b);

    printMat(a);
    printMat(b);

    return 0;
}