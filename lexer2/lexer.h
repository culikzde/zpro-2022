

#ifndef LEXER_H
#define LEXER_H

#include <fstream>
#include <string>
using namespace std;

enum TokenKind { ident, number, text, separator, eos };

class Lexer
{
private:
    ifstream f;
    char ch;
    void nextChar();

public:
    TokenKind kind;
    string token;
    void nextToken();
    void error(string msg);

public:
    bool isSeparator(char c);
    void checkSeparator(char c);

    double readNumber();

public:
    Lexer(string fileName);
    ~Lexer();
};

#endif // LEXER_H
