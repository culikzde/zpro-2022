#include <iostream>
using namespace std;

const int N = 3;

void tisk (string name, double p[N][N])
{
    cout << name << " = " << endl;
    cout << "{" << endl;
    for (int i = 0; i < N; i++)
    {
        cout << "   [" ;
        for (int k = 0; k < N; k++)
        {
            cout << p[i][k];
            if (k < N - 1) { cout << ", "; }
        }
        cout << "]";
        if (i < N - 1) { cout << ", "; }
        cout << endl;
    }
    cout << "}" << endl;
}

void zero (double p[N][N])
{
    for (int i = 0; i < N; i++)
    {
        for (int k = 0; k < N; k++)
        {
            p[i][k] = 0;
        }
    }
}

void one (double p[N][N])
{
    for (int i = 0; i < N; i++)
    {
        for (int k = 0; k < N; k++)
        {
            p[i][k] = i==k ? 1 : 0;
        }
    }
}

void all (double p[N][N], double value)
{
    for (int i = 0; i < N; i++)
    {
        for (int k = 0; k < N; k++)
        {
            p[i][k] = value;
        }
    }
}

void add (double v[N][N], double x[N][N], double y[N][N]) // v := x + y
{
    for (int i = 0; i < N; i++)
    {
        for (int k = 0; k < N; k++)
        {
            v[i][k] = x[i][k] + y[i][k];
        }
    }
}


double a[N][N];

double b[N][N];
double c[N][N];

int main()
{
    one (a);
    all (b, 2);
    add(c, a, b); // c := a+b
    tisk ("a", a);
    tisk ("b", b);
    tisk ("c", c);
}

