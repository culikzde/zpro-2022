#include <iostream>
using namespace std;

int a[3] = { 10, 20, 30 };
int b[3] = { 100, 200, 300 };
int v[3];

void print (string name, int pole[])
{
    cout << name << " = {";
    for (int i = 0; i < 3; i++)
    {
        cout << pole[i];
        if (i < 2) cout << ", ";
    }
    cout << "}" << endl;

}

void main()
{
    for (int i = 0; i < 3; i++)
    {
        v[i] = a[i] + b[i];
    }
    print("a", a);
    print("b", b);
    print("v", v);

}

