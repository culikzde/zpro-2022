#include <iostream>
#include <cassert>
using namespace std;

class Item;

class List
{
private:
    Item* first;
    Item* last;
    void insert (Item* before, Item* fresh, Item* after);

public:
    List() { first = nullptr; last = nullptr; }
    ~List();

    Item* getFirst() { return first; }
    Item* getLast() { return last; }

    void insertFirst (Item* p);
    void insertLast (Item* p);

    Item* search(string name) const;
    void print() const;

    friend class Item;
};

class Item
{
public:
    string name;
    int r, g, b;

private:
    List* link;
    Item* prev;
    Item* next;

public:
    Item* getPrev() { return prev; }
    Item* getNext() { return next; }

    Item(string name0 = "", int r0 = 0, int g0 = 0, int b0 = 0)
    {
        name = name0;  r = r0; g = g0; b = b0; link = nullptr;  prev = nullptr; next = nullptr;
    }

    ~Item();

    void insertPrev (Item* fresh);
    void insertNext (Item* fresh);

    void remove ();

    friend class List;
};

List::~List()
{
    Item* p = first;
    while (p != nullptr)
    {
        Item* t = p->next; // ukazatel na nasledujici prvek

        p->link = nullptr;
        p->prev = nullptr;
        p->next = nullptr;
        delete p;

        p = t;
    }
}

void List::insert (Item* before, Item* fresh, Item* after)
{
    assert(fresh != nullptr);
    assert(fresh->link == nullptr);
    assert(fresh->prev == nullptr);
    assert(fresh->next == nullptr);

    fresh->link = this;
    fresh->prev = before;
    fresh->next = after;

    if (before == nullptr)
        first = fresh;
    else
        before->next = fresh;

    if (after == nullptr)
        last = fresh;
    else
        after->prev = fresh;
}

void Item::remove()
{
    if (link != nullptr)
    {
        Item* before = prev;
        Item* after = next;
        
        if (before != nullptr)
            before->next = after;
        else
            link->first = after;

        if (after != nullptr)
            after->prev = before;
        else
            link->last = before;

        link = nullptr;
        prev = nullptr;
        next = nullptr;
    }
}

Item::~Item()
{
    if (link != nullptr)
        remove ();
}

void List::insertFirst (Item* p)
{
    insert (nullptr, p, first);
}

void List::insertLast (Item* p)
{
    this->insert (this->last, p, nullptr);
}

void Item::insertPrev (Item* fresh)
{
    assert(this != nullptr);
    assert(link != nullptr);
    link->insert (prev, fresh, this);
}

void Item::insertNext(Item* fresh)
{
    assert(this != nullptr);
    assert(link != nullptr);
    this->link->insert (this, fresh, this->next);
}

Item* List::search(string name) const
{
    Item* p = first;
    while (p != nullptr && p->name != name)
    {
        p = p->next;
    }
    return p;
}

void List::print () const
{
    cout << "seznam:" << endl;
    Item* p = first;
    while (p != nullptr)
    {
        cout << p->name << " (" << p->r << ", " << p->g << ", " << p->b << ")" << endl;
        // cout << "insertLast (new Item (" << p->name << ", " << p->r << ", " << p->g << ", " << p->b << "));" << endl;
        p = p->next;
    }
    cout << "konec seznamu" << endl;
    cout << endl;
}

List b;

void test()
{
    Item* t = new Item("cervena", 255, 0, 0);
    b.insertFirst(t);
    b.insertFirst(new Item("modra", 0, 0, 255));
    b.insertLast(new Item("zelena", 0, 255, 0));
    t->insertPrev(new Item("zluta", 255, 255, 0));
    t->insertNext(new Item("oranzova", 0xff, 0xa5, 0));

    Item* m = b.search("modra");
    assert(m != nullptr);
    m->insertPrev(new Item("fialova", 255, 0, 255));

    Item* z = b.search("zelena");
    assert(z != nullptr);
    z->insertNext(new Item("hneda", 255, 255, 0));

    Item s ("svetle zelena", 128, 255, 128);
    b.insertLast (&s);

    b.print();
}

int main ()
{ 
    test();
    b.print();
    cout << "O.K." << endl;
}

