#include <iostream>
using namespace std;

const int N = 21;

int a[N];

inline int na_druhou(int k)
{
    return k * k;
}

void vytiskni()
{
    for (int i = 0; i < N; i++)
    {
        if (i <= 9 || i >= N - 10)
        {
            cout << "a[" << i << "] = " << a[i] << endl;
        }
        if (i == 10 && N > 20)
        {
            cout << "..." << endl;
        }
    }
}

int secti()
{
    int sum = 0;
    for (int i = 0; i < N; i++)
    {
        sum += a[i];
        // sum = sum + a[i];
    }
    cout << "soucet = " << sum << endl;
    return sum;
}

void napln ();

int fib0(int n)
{
    if (n == 0)
        return 0;
    else if (n == 1)
        return 1;
    else
        return fib0(n - 1) + fib0(n - 2);
}

int fib (int vstup)
{
    int nejstarsi = 0; // starsi fib(0)
    int predchozi = 1; // predchozi fib(1)

    int vysledek = -1;
    if (vstup == 0)
    {
        vysledek = nejstarsi;
    }
    else if (vstup == 1)
    {
        vysledek = predchozi;
    }
    else
    {
        int index = 2; 
        while (index <= vstup)
        {
            vysledek = nejstarsi + predchozi; // nove
            nejstarsi = predchozi;
            predchozi = vysledek;
            index ++;
        }
    }
    return vysledek;
}


int main()
{
    for (int i = 0; i <= 10; i++) cout << fib0(i) << ", ";
    cout << endl;

    for (int i = 0; i <= 10; i++) cout << fib(i) << ", ";
    cout << endl;

    /*
    napln();
    vytiskni();
    secti();
    // int v = secti();
    */

    cout << "O.K.";
}

void napln ()
{
    for (int i = 0; i < N; i++)
    {
        a[i] = na_druhou(i + 1);
    }
}

