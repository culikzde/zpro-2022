
#include <iostream>
using namespace std;

const int N = 3;

void tisk(string name, double p [])
{
    cout << name << " = (";
    for (int i = 0; i < N; i++)
    {
        cout << p[i];
        if (i < N - 1) { cout << ", "; }
    }
    cout << ")" << endl;
}


double a[N] = { 1,  0,  0 };
double b[N] = { 0 , 1,  0 };

double c[N];

int main()
{
    // c := a + b
    for (int i = 0; i < N; i++)
    {
        c[i] = a[i] + b[i];
    }

    double dot = 0;
    for (int i = 0; i < N; i++)
    {
        dot += a[i] * b[i];
    }

    c[0] = a[1] * b[2] - b[1] * a[2];
    c[1] = a[2] * b[0] - b[2] * a[0];
    c[2] = a[0] * b[1] - b[0] * a[1];

    tisk ("acko", a);
    tisk ("b", b);
    tisk ("c", c);
    cout << "O.K." << endl;
}

