#include <iostream>
using namespace std;


struct Item
{
    string name;
    int r;
    int g;
    int b;
    Item * next;

    Item() { name = ""; r = 0; g = 0; b = 0; next = nullptr; }
    Item(string name0, int r0, int b0, int g0);

    void add (int r0, int g0, int b0);
    void svetlejsi() { name = "svetle " + name;  add(128, 128, 128); }
    void tisk ();
};

Item::Item (string name0, int r0, int b0, int g0) :
    name (name0),
    r (r0),
    g (g0), 
    b (b0), 
    next (nullptr)
{
}

void Item::tisk()
{
    cout << name << "(" << r << "," << g << "," << b << ")" << endl;
}

void Item::add(int r0, int g0, int b0)
{
    r += r0;
    g = g + g0;
    b = b + b0;

    if (r < 0) r = 0;
    if (r > 255) r = 255;

    if (g < 0) g = 0;
    if (g > 255) g = 255;

    if (b < 0) b = 0;
    if (b > 255) b = 255;
}

void pokus()
{
    Item* cervena = new Item;
    cervena->name = "cervena";
    cervena->r = 255;
    cervena->g = 0;
    cervena->b = 0;

    // cervena->next = nullptr;
    cervena->tisk();
    // cervena->add (0, 128, 0);
    cervena->svetlejsi();
    cervena->tisk();
    cervena->svetlejsi();
    cervena->tisk();

    Item* zelena = new Item("zelena", 0, 255, 0);
    zelena->tisk();
    zelena->svetlejsi();
    zelena->tisk();
}

Item * first = nullptr;

void print (Item * t)
{
    cout << t->name << "(" << t->r << "," << t->g << "," << t->b << ")" << endl;
}

void printAll (Item* f)
{
    while (f != nullptr)
    {
        print (f);
        f = f->next;
    }
}

void insertFirst(Item* p)
{
    p->next = first;
    first = p;
}

void insertLast (Item* p)
{
    if (first == nullptr)
    {
        first = p;
    }
    else
    {
        Item* f = first;
        while (f->next != nullptr) { f = f->next; }

        f->next = p;
    }
    p->next = nullptr;
}

void InsertFirst(Item*& start, Item* p)
{
    p->next = start;
    start = p;
}

Item * RemoveFirst(Item * & start)
{
    Item* result = nullptr;
    if (start != nullptr)
    {
        result = start;
        start = start->next; // posunu hlavicku
        result->next = nullptr; // odpojit
    }
    return result;
}

void InsertLast (Item * & start, Item* p)
{
    if (start == nullptr)
    {
        start = p;
    }
    else
    {
        Item* f = start;
        while (f->next != nullptr) { f = f->next; }

        f->next = p;
    }
    p->next = nullptr;
}


int main()
{
    Item* orange = new Item("oranzova", 255, 165, 0);
    Item * cervena = new Item("cervena", 255, 0, 0);
    Item * z = new Item ("zelena", 0, 0, 255);
    Item * m = new Item ("modra", 0, 0, 255);
    
    first = cervena;
    cervena->next = orange;
    orange->next = z;
    z->next = m;
    m->next = nullptr;
    // first->next->next->next = nullptr;

    printAll (first);
    // insertFirst(new Item("bila", 255, 255, 255));
    // insertLast(new Item("zluta", 255, 128, 0));
    InsertFirst(first, new Item("bila", 255, 255, 255));
    InsertLast(first, new Item("zluta", 255, 128, 0));

    cout << endl;
    printAll(first);

    Item* t = nullptr; // novy seznam
    while (first != nullptr)
    {
        InsertFirst (t, RemoveFirst (first));
    }

    cout << endl;
    cout << "Puvodni seznam" << endl;
    printAll(first);

    cout << endl;
    cout << "Novy seznam" << endl;
    printAll(t);

    cout << "O.K." << endl;
}
