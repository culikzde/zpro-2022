#include "seznam2.h"

#include <iostream>
#include <cassert>
using namespace std;

namespace My
{

    List::~List()
    {
        Item* p = first;
        while (p != nullptr)
        {
            Item* t = p->next;
            delete p;
            p = t;
        }
    }

    void insert(List* a, Item* before, Item* fresh, Item* after)
    {
        assert(fresh != nullptr);
        assert(fresh->link == nullptr);
        assert(fresh->prev == nullptr);
        assert(fresh->next == nullptr);

        fresh->link = a;
        fresh->prev = before;
        fresh->next = after;

        if (before == nullptr)
            a->first = fresh;
        else
            before->next = fresh;

        if (after == nullptr)
            a->last = fresh;
        else
            after->prev = fresh;
    }

    void insertFirst(List& a, Item* p)
    {
        insert(&a, nullptr, p, a.first);
    }

    void insertLast(List& a, Item* p)
    {
        insert(&a, a.last, p, nullptr);
    }

    void insertPrev(Item* old, Item* fresh)
    {
        assert(old != nullptr);
        insert(old->link, old->prev, fresh, old);
    }

    void insertNext(Item* old, Item* fresh)
    {
        assert(old != nullptr);
        insert(old->link, old, fresh, old->next);
    }

    Item* search(List& a, string name)
    {
        Item* p = a.first;
        while (p != nullptr && p->name != name)
        {
            p = p->next;
        }
        return p;
    }
    void print(const List& a)
    {
        cout << "seznam:" << endl;
        Item* p = a.first;
        while (p != nullptr)
        {
            cout << p->name << " (" << p->r << ", " << p->g << ", " << p->b << ")" << endl;
            // cout << "insertLast (new Item (" << p->name << ", " << p->r << ", " << p->g << ", " << p->b << "));" << endl;
            p = p->next;
        }
        cout << "konec seznamu" << endl;
        cout << endl;
    }

} // end of namespace My

using  namespace My;

int main()
{

    My::List b;
    My::Item* t = new My::Item("cervena", 255, 0, 0);
    My::insertFirst (b, t);
    
    insertFirst(b, new Item("modra", 0, 0, 255));
    insertLast(b, new Item("zelena", 0, 255, 0));
    insertPrev (t, new Item("zluta", 255, 255, 0));
    insertNext (t, new Item("oranzova", 0xff, 0xa5, 0));

    Item* m = search(b, "modra");
    assert (m != nullptr);
    insertPrev(m, new Item("fialova", 255, 0, 255));

    Item* z = search(b, "zelena");
    assert (z != nullptr);
    insertNext (z, new Item ("hneda", 255, 255, 0));

    print (b);
    
    cout << "O.K." << endl;
}

