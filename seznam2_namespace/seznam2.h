#pragma once

#include <string>
using namespace std;

namespace Foreign
{
    int List = 0;
    double Item = 3.14;
}

namespace My
{

    struct Item;

    struct List
    {
        Item* first;
        Item* last;

        List() : first(nullptr), last(nullptr) { }
        ~List();
    };

    struct Item
    {
        string name;
        int r, g, b;

        List* link;
        Item* prev;
        Item* next;

        Item(string name0 = "", int r0 = 0, int g0 = 0, int b0 = 0)
        {
            name = name0;  r = r0; g = g0; b = b0;
            link = nullptr; prev = nullptr; next = nullptr;
        }
    };

} // end of namespace My