#include <iostream>
#include <iomanip>
using namespace std;

const int N = 3;

double a[N][N] = { {0, 7, 8},
                   {2, 2, 5},
                   {4, 4, 2} };

/*
double a[N][N] = { {1, 1, 5},
                   {2, 2, 4},
                   {4, 4, 4} };

*/

double b[N] = { 22, 15, 22 };

double v[N] = { 3, 2, 1 };

void dopocitej() // dopocitej b, aby v bylo resenim
{
    for (int i = 0; i < N; i++)
    {
        double sum = 0;
        for (int k = 0; k < N; k++)
        {
            sum += a[i][k] * v[k];
        }
        b[i] = sum;
    }
}

const string neznama [N] = { "x", "y", "z" };

void tisk ()
{
    // #include <iomanip>
    for (int i = 0; i < N; i++)
    {
        for (int k = 0; k < N; k++)
        {
            if (a[i][k] < 0)
            {
                cout << " - ";
            }
            else if (k > 0)
            {
                cout << " + ";
            }
            else
            {
                cout << "   ";

            }
            cout << setw(12);
            // cout << std::left;
            cout << abs(a[i][k]);
            cout << "*";
            cout << neznama [k];
        }
        cout << " = ";
        cout << b[i];
        cout << endl;
    }
    cout << endl;
}

void mult_line (int r, double factor)
// radku r vynasob cislem factor
{
    for (int k = 0; k < N; k++)
    {
        a[r][k] = a[r][k] * factor;
    }

    // b[r] = b[r] * factor;
    b[r] *= factor;

}

void sub_lines(int r1, int r2, double factor = 1.0)
// od radky r1 odecti radku r2 vynasobenou cislem factor
// a[r1][*] = a[r1][*] - factor * a[r2][*]
{
    for (int k = 0; k < N; k++)
    {
        a[r1][k] -= a[r2][k] * factor;
        // a[r1][k] = a[r1][k] - a[r2][k] * factor;
    }

    b[r1] -= b[r2] * factor;
}

void swap_lines (int r1, int r2)
// prehodit radky r1 a r2
{
    double t;
    for (int k = 0; k < N; k++)
    {
        t = a[r1][k];
        a[r1][k] = a[r2][k];
        a[r2][k] = t;
    }

    t = b[r1];
    b[r1] = b[r2];
    b[r2] = t;
}

void uprav(int r, int s)
{
    mult_line (r, 1.0 / a[r][s]); // a[r][s] := 1
    for (int i = 0; i < N; i++)
    {
        if (i != r)
        {
            sub_lines(i, r, a[i][s]);
        }
    }
}

void simple()
{
    for (int i = 0; i < N; i++)
    {
        uprav(i, i);
        tisk();
    }
}

const double eps = 1e-12;

void complicated ()
{
    int s = 0;
    int r = 0;
    while (r < N && s < N)
    {
        bool ok = true;
        if (abs(a[r][s]) < eps)
        {
            cout << "Prilis male " << a[r][s] << endl;
            ok = false;
            double m = 0; // maximum "pod" a[r][s]
            int inx = -1; // cislo radky
            for (int i = r + 1; i < N; i++)
            {
                if (abs(a[i][s]) > m)
                {
                    m = abs(a[i][s]);
                    inx = i;
                }
            }
            if (m > eps)
            {
                cout << "Prehazuji " << r << " a " << inx << endl;
                swap_lines (r, inx);
                tisk ();
                ok = true;
            }
        }
        if (ok)
        {
            uprav (r, s);
            r ++;
            s ++;
        }
        else
        {
            cout << "Preskocim sloupec " << s << endl;
            s++; // preskocim sloupec
        }
        tisk();
    }
}

int main()
{
    // dopocitej();
    tisk ();
    // mult_line (0, 1.0 / 3);
    // simple();
    complicated();
    // tisk ();
}

