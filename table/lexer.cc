#include "lexer.h"

#include <iostream>
using namespace std;

const char zero = 0;

Lexer::Lexer (string fileName)
{
    f.open (fileName);
    if (not f.good ())
        error ("Cannot open file: " + fileName);

    ch = zero;
    nextChar ();
    nextToken ();
}

Lexer::~Lexer ()
{
    if (f.good ())
        f.close ();
}

void Lexer::error (string msg)
{
    cerr << "Error: " << msg << ", token=" << token << endl;
    exit (1);
}

void Lexer::nextChar ()
{
    f >> ch;
    if (!f.good ())
        ch = zero;
}

void Lexer::digits ()
{
    while (ch >= '0' && ch <= '9')
    {
        token = token + ch;
        nextChar ();
    }
}

void Lexer::nextToken ()
{
    token = "";
    while (ch != zero && ch <= ' ')
        nextChar ();

    if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
    {
        kind = ident;
        while (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
        {
            token = token + ch;
            nextChar ();
        }
    }
    else if (ch >= '0' && ch <= '9')
    {
        kind = number;
        digits ();
        if (ch == '.')
        {
            token = token + ch;
            nextChar ();
            digits ();
        }
        if (ch == 'e' || ch == 'E')
        {
            token = token + ch;
            nextChar ();
            if (ch == '+' || ch == '-')
            {
                token = token + ch;
                nextChar ();
            }
            digits ();
        }
    }
    else if (ch != zero)
    {
        kind = separator;
        token = string (1, ch);
        nextChar ();
    }
    else
    {
        kind = eos;
    }
}

bool Lexer::isSeparator (char c)
{
    return kind == separator && token == string (1, c);
}

void Lexer::checkSeparator (char c)
{
    if (!isSeparator (c))
        error (string (1, c) + " expected");
    nextToken ();
}

double Lexer::readNumber ()
{
    string txt = "";

    if (isSeparator ('-'))
    {
        txt = "-";
        nextToken ();
    }

    if (kind != number)
        error ("Number expected");
    txt = txt + token;

    double result = stod (txt);

    nextToken ();
    return result;
}

/* ---------------------------------------------------------------------- */

Mat readMat (string fileName)
{
    Mat result;
    Lexer inp (fileName);
    int col = -1;

    inp.checkSeparator ('{');
    while (!inp.isSeparator ('}'))
    {
        inp.checkSeparator ('[');

        Vec v;
        while (!inp.isSeparator (']'))
        {
            double num = inp.readNumber ();
            v.push_back (num);

            // if (inp.isSeparator (','))
            if (!inp.isSeparator (']'))
                inp.checkSeparator (',');
        }
        inp.checkSeparator (']');
        result.push_back (v);

        if (col != -1 && col != v.size ())
            inp.error ("Different number of columns");
        col = v.size ();

        if (inp.isSeparator (','))
            inp.nextToken ();
    }
    inp.checkSeparator ('}');
    if (col == -1 && col == 0)
        inp.error ("Strange data");
    return result;
}

/* ---------------------------------------------------------------------- */

void writeMat (string fileName, const Mat mat)
{
    ofstream f (fileName);

    int n = mat.size ();
    f << "{" << endl;
    for (int i = 0; i < n; i++)
    {
        f << "   [" ;
        int s = mat[i].size ();
        for (int k = 0; k < s; k++)
        {
            f << mat[i][k];
            if (k < s-1) f << ", ";
        }
        f << "]";
        if (i < n-1) f << ",";
        f << endl;
    }
    f << "}" << endl;
    f << endl;

    f.close ();
}

/* ---------------------------------------------------------------------- */

