#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi (this);
    ui->splitter->setStretchFactor (0, 3);
    ui->splitter->setStretchFactor (1, 1);

    ui->tabWidget->clear ();

    table = new QTableWidget;
    ui->tabWidget->addTab (table, "Entry");
    QObject::connect (table, &QTableWidget::cellChanged,
                      this, &MainWindow::on_tableWidget_cellChanged);

    int n = ui->lineSpinBox->value();
    int m = ui->columnSpinBox->value();
    // table->setRowCount (n);
    // table->setColumnCount (m);


    // Mat mat (n, Vec (m, 7));
    // data = mat;

    /*
    data.clear();
    for (int i = 0 ; i < n; i++)
    {
        Vec v;
        for (int k = 0 ; k < m; k++)
        {
            double x = 0;
            if (i == k) x = 1.1;
               v.push_back (x);
        }
        data.push_back (v);
    }
    */
    data = Mat (n, Vec (m, 0.0));
    for (int i = 0 ; i < n; i++)
    {
        for (int k = 0 ; k < m; k++)
        {
            double x = 0;
            if (i == k) x = 1.1 * k;
               data[i][k] = x;
        }
    }

    displayData ();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::info (QString txt)
{
   ui->output->append (txt);
}

void MainWindow::changeTable ()
{
    int n = ui->lineSpinBox->value();
    int m = ui->columnSpinBox->value();
    // info ("change table " + QString::number (n) + " x " + QString::number (m));

    Mat mat (n, Vec (m, 0)); // nulova matice
    for (int i = 0 ; i < n; i++)
        if (i < m)
            mat [i][i] = 1; // jednotkova

    int old_n = data.size ();
    int old_m = data[0].size ();
    for (int i = 0 ; i < n; i++)
    {
        if (i < old_n)
            for (int k = 0 ; k < m; k++)
            {
                if (k < old_m)
                   mat[i][k] = data [i][k];
            }
    }

    data = mat;
    displayData ();
}

void MainWindow::on_lineSpinBox_valueChanged(int arg1)
{
    if (enable_change)
       changeTable ();
}


void MainWindow::on_columnSpinBox_valueChanged(int arg1)
{
    if (enable_change)
       changeTable ();
}

void MainWindow::on_tableWidget_cellChanged (int i, int k)
{
    QTableWidgetItem * item = table->item (i, k);
    QString value = item->text();
    value = value.simplified();
    double x = value.toDouble();
    data[i][k] = x;
    value = QString::number (x);
    item->setText (value); // pro kontrolu

    /*
    info ("change item " +
             QString::number (i) + ", " + QString::number (k) +
             " to " + value);
             */
}

void MainWindow::displayData ()
{
    int n = data.size ();
    int m = data[0].size ();

    table->clear();
    table->setRowCount (n);
    table->setColumnCount (m);

    enable_change = false;
    ui->lineSpinBox->setValue (n);
    ui->columnSpinBox->setValue (m);
    enable_change = true;

    for (int i = 0 ; i < n; i++)
    {
        for (int k = 0 ; k < m; k++)
        {
            double x = data [i][k];
            QTableWidgetItem * item = new QTableWidgetItem;

            item->setText (QString::number (x));
            item->setToolTip ("(" + QString::number (i) + "," + QString::number (k) + ")");

            QColor color = QColor ("blue");
            if (i == k) color = QColor ("orange");
            item->setForeground (color);

            table->setItem(i, k, item);
        }
    }
}

void MainWindow::copyTable (QString title)
{
    QTableWidget * t = new QTableWidget;
    ui->tabWidget->addTab (t, title);

    /* copy of table */
    int n = table->rowCount ();
    int m = table->columnCount ();
    t->setRowCount (n);
    t->setColumnCount (m);

    for (int i = 0 ; i < n; i++)
    {
        for (int k = 0 ; k < m; k++)
        {
            QTableWidgetItem * item = table->item (i, k);
            if (item != nullptr)
            {
               QTableWidgetItem * p = new QTableWidgetItem;
               p->setText (item->text());
               p->setToolTip (item->toolTip());
               p->setForeground (item->foreground());
               p->setBackground (item->background());
               t->setItem (i, k, p);
            }
        }
    }
}

/*
void MainWindow::readJson (QByteArray code)
{
    QJsonDocument doc = QJsonDocument::fromJson (code);
    QJsonObject obj = doc.object ();

    data.clear ();
    if (obj.contains ("items") && obj ["items"].isArray())
    {
        QJsonArray list = obj ["items"].toArray ();
        for (QJsonValue block : list)
        {
            if (block.isArray ())
            {
               QJsonArray line = block.toArray();
               Vec v;
               for (QJsonValue item : line)
               {
                   if (item.isDouble())
                   {
                       double x = item.toDouble();
                       v.push_back(x);
                   }
               }
               data.push_back (v);
            }
        }
    }
}

QByteArray MainWindow::writeJson ()
{
    QJsonObject obj;
    QJsonArray list; // cela matice

    int n = data.size ();
    int m = data[0].size ();

    for (int i = 0 ; i < n; i++)
    {
        QJsonArray line;
        for (int k = 0 ; k < m; k++)
        {
           QJsonValue value = data [i][k];
           line.append (value);
        }
        list.append(line);
    }

    obj ["items"] = list;

    QJsonDocument doc (obj);
    QByteArray code = doc.toJson ();
    return code;
}
*/

Mat readMat (QString fileName)
{
    return readMat (fileName.toStdString());
}

void writeMat (QString fileName, const Mat mat)
{
   writeMat (fileName.toStdString(), mat);
}

void MainWindow::openFile (QString fileName)
{
    data = readMat (fileName);
    displayData ();
    /*
    QFile f (fileName);
    if (f.open (QFile::ReadOnly))
    {
        QByteArray code = f.readAll ();
        readJson (code);
        displayData ();
        f.close ();
    }
    else
    {
       QMessageBox::warning (NULL, "Open File Error", "Cannot read file: " + fileName);
    }
    */
}

void MainWindow::saveFile (QString fileName)
{
    writeMat (fileName, data);
    /*
    QFile f (fileName);
    if (f.open (QFile::WriteOnly))
    {
        QByteArray code = writeJson ();
        f.write (code);
        f.close ();
    }
    else
    {
       QMessageBox::warning (NULL, "Save File Error", "Cannot write file: " + fileName);
    }
    */
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open file");
    if (fileName != "")
        openFile (fileName);
}

void MainWindow::on_actionSave_triggered()
{
   QString fileName = QFileDialog::getSaveFileName (this, "Save file");     if (fileName != "")
   if (fileName != "")
      saveFile (fileName);
}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}

void MainWindow::on_actionRun_triggered()
{
    run ();
}

void MainWindow::on_pushButton_clicked()
{
    run ();
}

void MainWindow::run()
{
    copyTable ("copy");
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}







