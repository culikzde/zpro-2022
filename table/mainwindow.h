#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "precompiled.h"
#include "lexer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void displayData ();
    void copyTable (QString title = "");

    void openFile (QString fileName);
    void saveFile (QString fileName);

    void run ();

    // void readJson (QByteArray code);
    // QByteArray writeJson();

private slots:
    void on_actionSave_triggered();
    void on_actionOpen_triggered();
    void on_actionRun_triggered();
    void on_actionQuit_triggered();

    void on_lineSpinBox_valueChanged(int arg1);
    void on_columnSpinBox_valueChanged(int arg1);
    void on_tableWidget_cellChanged (int row, int column);

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QTableWidget * table = nullptr;
    Mat data;
    bool enable_change = false;
    void changeTable ();
    void info (QString txt);
};

#endif // MAINWINDOW_H
