QT       += core gui widgets

CONFIG   += c++11 precompile_header

PRECOMPILED_HEADER += precompiled.h

SOURCES +=  mainwindow.cc \
    lexer.cc

HEADERS += mainwindow.h precompiled.h \
    lexer.h

FORMS += mainwindow.ui

