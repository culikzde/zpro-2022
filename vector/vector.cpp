#include <vector>
#include <iostream>
using namespace std;

int main()
{
    vector <double> v;

    vector < vector <double> > m;

    v.push_back(10.0);
    v.push_back(22.2);
    v.push_back(33.3);

    int cnt = v.size();
    for (int i = 0; i < cnt; i++)
        cout << "v[" << i << "] = " << v[i] << endl;

    cout << "O.K." << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
